import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import {HomePage} from "../pages/Home/home";
import {AskTheExpertPage} from "../pages/ask-the-expert/ask-the-expert";
import {QuizPage} from "../pages/quiz/quiz";
import {MyNotificationPage} from "../pages/my-notification/my-notification";
import { TranslateService } from '@ngx-translate/core';
import {ChangeLanguagePage} from "../pages/change-language/change-language";
import {StartAppPage} from "../pages/start-app/start-app";
import { Facebook } from '@ionic-native/facebook';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { AlertController} from 'ionic-angular';
import {StartQuizPage} from "../pages/start-quiz/start-quiz";
import {MyProfilePage} from "../pages/my-profile/my-profile";
import {App} from "ionic-angular/index";



export interface PageInterface {
  title: string;
  name: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  tabComponent?: any;
}

@Component({
  templateUrl: 'app.template.html'
})

export class TSTA_App {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;
  p:any;
  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu
  appPages: PageInterface[] = [
    { title: 'Home', name: 'TabsPage', component: TabsPage, tabComponent: HomePage, index: 0, icon: 'calendar' },
    { title: 'Ask the Expert', name: 'TabsPage', component: TabsPage, tabComponent: AskTheExpertPage, index: 1, icon: 'contacts' },
    { title: 'My Notification', name: 'TabsPage', component: TabsPage, tabComponent: MyNotificationPage, index: 2, icon: 'information-circle' },
    { title: 'Quiz', name: 'TabsPage', component: TabsPage, tabComponent: StartQuizPage, index: 3, icon: 'map' },
    { title: 'My Profile', name: 'TabsPage', component: TabsPage, tabComponent: MyProfilePage, index: 4, icon: 'information-circle' }
  ];

  /*loggedInPages: PageInterface[] = [
    { title: 'ChangeLanguage', name: 'ChangeLanguagePage', component: ChangeLanguagePage, icon: 'person' },
    { title: 'Logout', name: 'StartAppPage', component: StartAppPage, icon: 'log-out', logsOut: true }
  ];

  loggedOutPages: PageInterface[] = [
    { title: 'ChangeLanguage', name: 'ChangeLanguagePage', component: ChangeLanguagePage, icon: 'person' },
    { title: 'Login', name: 'StartAppPage', component: StartAppPage,  icon: 'log-in' }
  ];*/
  rootPage: any;
  isLoggedIn:boolean = false;

  constructor(
    public events: Events,
    public userData: UserData,
    public menu: MenuController,
    public platform: Platform,
    public confData: ConferenceData,
    public storage: Storage,
    public splashScreen: SplashScreen,
    private fb: Facebook,
    translate: TranslateService,
    public push: Push,
    public alertCtrl: AlertController,
    public app: App

  ) {
    this.enableMenu(true);
    this.initializeApp();
      // this.userData.changeLng('en');
      this.userData.getLanguage().then((language) => {
      if(language==null || language=='' ){
        translate.setDefaultLang('en');
      }
      else{
            translate.setDefaultLang(language);
        }
      });

    this.storage.get('hasLoggedIn')
      .then((hasLoggedIn) => {
        if (hasLoggedIn) {
          this.menu.enable(true);
          this.rootPage = TabsPage;
        } else {
          this.menu.enable(false);
          this.rootPage = StartAppPage;
        }
      //  this.platformReady()
      });

    // load the conference data
   // confData.load();

    // decide which menu items should be hidden by current login status stored in local storage
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      this.enableMenu(hasLoggedIn === true);
    });
    this.enableMenu(true);
    this.listenToLoginEvents();
  }


  /* tab1Root: any = HomePage;
  tab2Root: any = MyQuestionsPage;
  tab3Root: any = StartQuizPage;
  tab4Root: any = MyProfilePage;
  tab5Root: any = AskTheExpertPage;*/



  openPage1(){
    /*this.p= this.appPages[0];
    this.openPage(this.p);
    this.isActive(this.p);*/
    let nav = this.app.getRootNav();
    nav.setRoot(TabsPage, {tabIndex: 0});
  }

  openPage2(){
   /* this.p= this.appPages[1];
    this.openPage(this.p);
    this.isActive(this.p);*/

    let nav = this.app.getRootNav();
    nav.setRoot(TabsPage, {tabIndex: 1});
  }

  openPage3(){
/*    this.p= this.appPages[2];
    this.openPage(this.p);
    this.isActive(this.p);*/

    let nav = this.app.getRootNav();
    nav.setRoot(TabsPage, {tabIndex: 2});
  }

  openPage4(){
   /* this.p= this.appPages[3];
    this.openPage(this.p);
    this.isActive(this.p);*/
    let nav = this.app.getRootNav();
    nav.setRoot(TabsPage, {tabIndex: 3});
  }

  openPage5(){
   /* this.p= this.appPages[4];
    this.openPage(this.p);
    this.isActive(this.p);*/
    let nav = this.app.getRootNav();
    nav.setRoot(TabsPage, {tabIndex: 4});
  }


  changeLanguage(){
    this.nav.setRoot(ChangeLanguagePage);
  }

  logOut(){
    this.userData.logout();
    this.nav.setRoot(StartAppPage);
    this.fblogout();
  }


  fblogout() {
    this.fb.logout()
        .then( res => this.isLoggedIn = false)
        .catch(e => console.log('Error logout from Facebook', e));
  }


    signupCLick(){
        this.nav.setRoot(SignupPage);
    }


/*
  openPage(page: PageInterface) {
         console.log()
       let params = {};
       if (page.index) {
         params = {tabIndex: page.index };
       }
       // If we are already on tabs just change the selected tab
       // don't setRoot again, this maintains the history stack of the
       // tabs even if changing them from the menu
       if (this.nav.getActiveChildNavs().length && page.index != undefined) {
         this.nav.getActiveChildNavs()[0].select(page.index);
       } else {
         // Set the root of the nav with params if it's a tab index
         console.log(page.name);
         this.nav.setRoot(page.name, params).catch((err: any) => {
           console.log(`Didn't set nav root: ${err}`);
         });
       }
    if (page.logsOut === true) {
      // Give the menu time to close before changing to logged out
      this.userData.logout();
    }
  }
*/



  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
      this.menu.enable(true);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.menu.enable(false);
    });

    this.events.subscribe('user:tryApp', () => {
      this.enableMenu(false);
    });
    }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
     this.menu.enable(!loggedIn, 'loggedOutMenu');
  }

  private initializeApp():void {
    this.platform.ready().then(() => {
      // do whatever you need to do here.
     // this.statusBar.styleDefault();
      this.initPushNotification();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    });
  }



  initPushNotification() {
    const options: PushOptions = {
      android: {
        senderID: '355508172562',
        sound: 'true',
        vibrate: true,
        forceShow: "1",
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);
    pushObject.on('registration').subscribe((data: any) => {
      this.storage.set('deviceToken',data.registrationId);
      //  alert('device token -> ' + data.registrationId);
      //TODO - send device token to server
    });

    pushObject.on('notification').subscribe((data: any) => {
      console.log('message -> ' + data.message);
      //if user using app and push notification comes
      if (data.additionalData.foreground) {
        // if application open, show popup
        let confirmAlert = this.alertCtrl.create({
          title: 'New Notification',
          message: data.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              //TODO: Your logic here
              // this.nav.push(DetailsPage, { message: data.message });
            }
          }]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        // this.nav.push(DetailsPage, { message: data.message });
        // console.log('Push notification clicked');
      }
    });
    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNavs()[0];
    // Tabs are a special case because they have their own navigation
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }
    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'primary';
    }
    return;
  }
}
