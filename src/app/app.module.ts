import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { TSTA_App } from './app.component';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { UserData } from '../providers/user-data';
import {ConferenceData} from "../providers/conference-data";
import {SessionDetailPage} from "../pages/session-detail/session-detail";
import {HomePage} from "../pages/Home/home";
import {AskTheExpertPage} from "../pages/ask-the-expert/ask-the-expert";
import {QuizPage} from "../pages/quiz/quiz";
import {MyNotificationPage} from "../pages/my-notification/my-notification";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {ChangeLanguagePage} from "../pages/change-language/change-language";
import {RestApi} from "../providers/rest_api";
import {StartAppPage} from "../pages/start-app/start-app";
import {InfoStoreDetailPage} from "../pages/info-store-detail/info-store-detail";
import { Facebook } from '@ionic-native/facebook';
import { Push } from '@ionic-native/push';
import { AlertController} from 'ionic-angular';
import {MyProfilePage} from "../pages/my-profile/my-profile";
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera } from '@ionic-native/camera';
import {StartQuizPage} from "../pages/start-quiz/start-quiz";
import {MyQuestionsPage} from "../pages/my-questions/my-questions";
import {InformationStorePage} from "../pages/information-store/information-store";





export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    TSTA_App,
    MyNotificationPage,
    AccountPage,
    LoginPage,
    HomePage,
    SignupPage,
    AskTheExpertPage,
    TabsPage,
    QuizPage,
    SessionDetailPage,
    StartAppPage,
    ChangeLanguagePage,
    InfoStoreDetailPage,
      MyProfilePage,
      StartQuizPage,
      MyQuestionsPage,
      InformationStorePage
  ],

  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
   IonicModule.forRoot(TSTA_App, {tabsHideOnSubPages:true} ),

/*    IonicModule.forRoot(TSTA_App, {tabsHideOnSubPages:true} ,{
      links: [{ component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
        { component: HomePage, name: 'Home', segment: 'home' },
        { component: AskTheExpertPage, name: 'AskTheExpertPage', segment: 'ask-the-expert' },
        { component: StartQuizPage, name: 'Quiz', segment: 'quiz' },
        { component: MyNotificationPage, name: 'MyNotification', segment: 'my-notification' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: AccountPage, name: 'AccountPage', segment: 'account' },
        { component: SignupPage, name: 'SignupPage', segment: 'signup' },
        { component: ChangeLanguagePage, name: 'ChangeLanguage ', segment: 'changeLanguage' },
        { component: StartAppPage, name: 'startApp' }
      ]
    }),*/
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TSTA_App,
    MyNotificationPage,
    AccountPage,
    LoginPage,
    QuizPage,
    SignupPage,
    TabsPage,
    HomePage,
    AskTheExpertPage,
    ChangeLanguagePage,
    StartAppPage,
    InfoStoreDetailPage,
      MyProfilePage,
      StartQuizPage,
      MyQuestionsPage,
      InformationStorePage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen,
    Facebook,
    RestApi,
    Push,
    AlertController,
    File,
    FileTransfer,
    Camera
  ]
})
export class AppModule { }
