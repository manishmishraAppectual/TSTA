import { Component } from '@angular/core';
import { NavController,MenuController, NavParams,Platform ,Nav } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import {ToastController} from "ionic-angular/index";
import {LoadingController} from "ionic-angular/index";
import {RestApi} from "../../providers/rest_api";
import {UserData} from "../../providers/user-data";
import {TabsPage} from "../tabs-page/tabs-page";
import {LoginPage} from "../login/login";
import {SignupPage} from "../signup/signup";
import { ViewController } from 'ionic-angular';



/**
 * Generated class for the StartAppPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-start-app',
  templateUrl: 'start-app.html',
})
export class StartAppPage {
  isLoggedIn:boolean = false;
  loading:any;
  users: any;
  count=0;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: Facebook,
              public loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              public restApi:RestApi,
              public userData: UserData,
              public menu: MenuController,
              public platform: Platform
  ) {
    fb.getLoginStatus()
        .then(res => {
          console.log(res.status);
          if(res.status === "connect") {
            this.isLoggedIn = true;
          } else {
            this.isLoggedIn = false;
          }
        })
        .catch(e => console.log(e));

    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
    });
  }

  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
      if (page == "StartAppPage") {
        this.platform.registerBackButtonAction(()=> {
          if (this.count == 1) {
            this.platform.exitApp();
          }
          this.myHandlerFunction()
        });
      }
    });
  }

    myHandlerFunction(){
      this.count++;
      setTimeout(() => {
        this.count--;
      }, 3000);
      let toast = this.toastCtrl.create({
        message: "Press Again to Confirm Exit",
        duration: 3000
      });
      toast.present();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartAppPage');
  }

    socilaLogin() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
        .then(res => {
          if(res.status === "connected") {
            this.isLoggedIn = true;
          //  alert(JSON.stringify(res));
            this.getUserDetail(res.authResponse.userID);
          } else {
            this.isLoggedIn = false;
            alert("this.isLoggedIn = false");
          }
        })
        .catch(e => alert('Error logging into Facebook'+e));
  }


  getUserDetail(userid) {
    this.fb.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
        .then(res => {
      //    alert(JSON.stringify(res));
          this.users = res;
          this.fbUserRegister(this.users);
        })
        .catch(e => {
          console.log(e);
        });
  }


  fbUserRegister(user) {
    this.showLoader();
    this.restApi.socilLogin(user).then((result) => {
    //    alert(JSON.stringify(result));
      if(result["error"] == false){
        this.loading.dismiss();
        this.userData.login(result["user"]);
        //noinspection TypeScriptValidateTypes
        this.navCtrl.push(TabsPage);
      }else{
        this.loading.dismiss();
        this.presentToast(result["message"]);
      }
    }, (err) => {
      this.loading.dismiss();
      this.presentToast(err);
    });
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Authenticating...'
    });
    this.loading.present();
    setTimeout(() => {
      this.loading.dismiss();
    }, 30000);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
    });
    toast.present();
  }




  loginPage(){
    this.navCtrl.setRoot(LoginPage);
  }


  signup(){
    this.navCtrl.setRoot(SignupPage);
  }


  tryApp(){
    this.userData.tryApp();
    this.navCtrl.setRoot(TabsPage);
   // this.menu.enable(true);
  }


  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }
  /*
  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }*/

}
