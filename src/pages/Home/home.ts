import { Component, ViewChild } from '@angular/core';
import { NavController,Nav,Platform} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import {RestApi} from "../../providers/rest_api";
import {LoadingController} from "ionic-angular/index";
import {InfoStoreDetailPage} from "../info-store-detail/info-store-detail";
import {ChangeLanguagePage} from "../change-language/change-language";
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';
import { ViewController } from 'ionic-angular';
import {ToastController} from "ionic-angular/index";
import {MyNotificationPage} from "../my-notification/my-notification";
import {Slides} from "ionic-angular/index";
import {InformationStorePage} from "../information-store/information-store";


let apikey='965b4ff1185114ad588b8e9e874523e2';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    @ViewChild(Slides) slides: Slides;

    @ViewChild(Nav) nav: Nav;
  userObj={"api_Key":""};
  loading:any;
  slider_data:any;
  lang:any;
  healthdata:any;
  lngID:any;
  apiKey:any;
  count=0;
  dataCount=0;

constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public translate: TranslateService,
    public restApi:RestApi,
    public userData: UserData,
    public storage: Storage,
    public platform: Platform,
    private toastCtrl: ToastController
) {
      this.userData.getLanguage().then((language) => {
      if(language==null || language=='' ){
        this.dashSlider('en');
       this.healthtipsData('en');
      }
      else{
       this.dashSlider(language);
       this.healthtipsData(language);
      }
    });

   this.userData.getUsername().then((val) => {
      if(val==null || val=='' ){
      this.apiKey=apikey;
      }
      else{
     // this.userObj=val;
      this.apiKey=val.api_Key;
      console.log(this.apiKey);
      }
    });
    this.getPageName();
 }

    getPageName() {
            this.navCtrl.viewDidEnter.subscribe(item=> {
         //       alert("1");
                const viewController = item as ViewController;
                const page = viewController.name;
                this.back_H_ButtonAction(page);
                this.dataCount++;
            });
      /* if(this.dataCount==0){
           this.dataCount++;
           alert("2");
           let page="HomePage";
           this.back_H_ButtonAction(page);
       }*/
    }


  back_H_ButtonAction(page) {
      this.dataCount--;
        this.platform.registerBackButtonAction(()=> {
            if (page == "HomePage") {
                if (this.count == 1) {
                    this.platform.exitApp();
                }
                this.myHandlerFunction()
            }
        });
  }

  myHandlerFunction(){
    this.count++;
    setTimeout(() => {
      this.count--;
    }, 3000);

    let toast = this.toastCtrl.create({
      message: "Press Again to Confirm Exit",
      duration: 3000
    });
    toast.present();
  }


    ionViewDidEnter() {
        this.slides.autoplayDisableOnInteraction = false;
    }

  ionViewDidLoad() {
      this.getPageName();
  }

  dashSlider(language) {
    this.restApi.dashSliderData(language,this.apiKey).then((result) => {
      if (result["error"] == false) {
        this.slider_data=result["sliderData"];
      }
    })
  }

  healthtipsData(language) {
    this.showLoader();
    this.restApi.healtTips(language,this.apiKey).then((result) => {
      this.loading.dismiss();
      if (result["error"] == false) {
        this.healthdata=result["healthTips"];
        console.log(result);
      }
    })
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
    setTimeout(() => {
      this.loading.dismiss();
    }, 30000);
  }

  storeDetail(){
   // this.navCtrl.goToRoot(InfoStoreDetailPage);
    this.navCtrl.push(InfoStoreDetailPage);
  }

    infoStore(data){
        this.navCtrl.push(InformationStorePage,{infoData:data});
    }


  notificationClick() {
      this.navCtrl.push(MyNotificationPage);
  }

}
