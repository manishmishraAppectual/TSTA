import { Component } from '@angular/core';
import {App} from "ionic-angular/index";
import {Platform} from "ionic-angular/index";
import {ViewController} from "ionic-angular/index";
import {TabsPage} from "../tabs-page/tabs-page";
import {NavController} from "ionic-angular/index";


@Component({
  selector: 'page-my-notification',
  templateUrl: 'my-notification.html'
})
export class MyNotificationPage {

  constructor(public navCtrl: NavController,
              public platform: Platform,
              public app: App) {
              this.getPageName();
  }

  getPageName() {
    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
    });
  }


  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
      if (page == "MyNotificationPage") {
        this.platform.registerBackButtonAction(()=> {
          let nav = this.app.getRootNav();
          nav.setRoot(TabsPage, {tabIndex: 0  });
        });
      }
    });
  }

  ionViewDidLoad() {
    this.getPageName();
  }


}
