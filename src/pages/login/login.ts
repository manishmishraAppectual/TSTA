import { Component,ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController,MenuController,Platform ,Nav} from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import { TabsPage } from '../tabs-page/tabs-page';
import { SignupPage } from '../signup/signup';
import {RestApi} from "../../providers/rest_api";
import {LoadingController} from "ionic-angular/index";
import {ToastController} from "ionic-angular/index";
import { Observable } from 'rxjs/Observable';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import {StartAppPage} from "../start-app/start-app";
import {  OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';
Nav

@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit {

  @ViewChild(Nav) nav: Nav;
  login: UserOptions = { email: '', password: '' };
  submitted = false;
  loading:any;
  loginForm: FormGroup;

  constructor(public navCtrl: NavController,
              public userData: UserData,
              public restApi:RestApi,
              public loadingCtrl:LoadingController,
              public toastCtrl:ToastController,
              public formBuilder: FormBuilder,
              public translate: TranslateService,
              public menu: MenuController,
              public platform: Platform
  ) {

    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);

    });
  }


  ionViewDidLoad() {
   // this.getActivePage();
  }

  back_H_ButtonAction(page){
    this.platform.ready().then(() => {
      this.platform.registerBackButtonAction(() => {
        if (page == "LoginPage") {
         this.navCtrl.setRoot(StartAppPage);
        } else {
          // go to previous page
          this.nav.pop({});
        }
      });
    });
  }



  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    //noinspection TypeScriptValidateTypes
    this.loginForm = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
    });
  }











  onLogin() {
    this.doLogin();
    //this.submitted = true;
    //  if (form.valid) {
       // this.userData.login(this.login.username);
      // this.navCtrl.push(TabsPage);
    //  }
   }

  doLogin(){
    console.log(this.login);
      this.showLoader();
      this.restApi.loginApi(this.login).then((result) => {
        console.log(result);
        this.loading.dismiss();
        if (result["error"] == false) {
          this.userData.login(result["user"]);
          this.navCtrl.push(TabsPage);

        }
        else{
          this.presentToast(result["message"]);
        }
      })
    }

    showLoader(){
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      this.loading.present();

      setTimeout(() => {
        this.loading.dismiss();
      }, 30000);
    }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.onDidDismiss(() => {
    });
    toast.present();
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }



  /*ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }



  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }*/
    backto_startapp(){
        this.navCtrl.push(StartAppPage);
    }
}
