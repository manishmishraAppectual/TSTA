import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import {FileTransfer, FileTransferObject} from "@ionic-native/file-transfer";
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AlertController, LoadingController, ToastController, ViewController} from 'ionic-angular';
import {isUndefined} from "util";
import {TabsPage} from "../tabs-page/tabs-page";
import {App} from "ionic-angular/index";

/**
 * Generated class for the MyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {


  submitAttempt : boolean = false;
  ImageHTTPURL : any = "http://172.104.54.149/toShytoAsk/even_rest_api/v1/edit_profile.php";
  imageNewPath: any;
  imageChosen: any = 0;
  profile_image : string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private camera: Camera,
              public files : File,
              public alertCtrl: AlertController,
              public fileTransfer : FileTransfer,
              public platform: Platform,
              public app: App) {

    this.getPageName();

  }

  getPageName() {
    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
    });
  }


  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
      if (page == "MyProfilePage") {
        this.platform.registerBackButtonAction(()=> {
          let nav = this.app.getRootNav();
          nav.setRoot(TabsPage, {tabIndex: 0  });
        });
      }
    });
  }

  ionViewDidLoad() {
    this.getPageName();
  }


  editProfile(){
    let confirm = this.alertCtrl.create({
      title: 'Image Selection',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            this.captureImage();
          }
        },
        {
          text: 'Gallary',
          handler: () => {
            this.imageFromGallary();
          }
        }
      ]
    });
    confirm.present();
  }


  private captureImage():void {

    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,


    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.profile_image = 'data:image/jpeg;base64,' +imageData;
      this.imageNewPath = imageData.nativeURL;
      this.imageChosen = 1;
     this.update2();
    }, (err) => {
      // Handle error
      alert(JSON.stringify(err));
    });
  }

  private imageFromGallary():void {

    const options: CameraOptions = {
      quality: 75,
      destinationType   : this.camera.DestinationType.DATA_URL,
      sourceType        : this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true

    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.profile_image = 'data:image/jpeg;base64,' +imageData;
      this.imageNewPath = imageData.nativeURL;
      this.imageChosen = 1;
      this.update2();
    }, (err) => {
      // Handle error
      alert(JSON.stringify(err));
    });
  }


  update2(){

/*    if(this.imageChosen!=0){
alert("f");
        if(!isUndefined(this.profile_image) ){*/

          let data = { user_id :'7184',first_name:'manish',last_name:'mishra', imageChosen : this.imageChosen, email_id : 'manish@appectual.com',mobile : '1234569870'};

          let timestamp = new Date().getUTCMilliseconds();
          let filename = 'tsta'+"_"+timestamp+".jpg";
          let options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            params: data,
          };

           alert("start");
   // this.profile_image = 'assets/img/ASK.jpg';
          const fileTransfer: FileTransferObject = this.fileTransfer.create();

          fileTransfer.upload(this.profile_image, 'http://172.104.54.149/toShytoAsk/even_rest_api/v1/edit_profile.php',
              options).then((entry) => {
            alert("data"+JSON.stringify(entry));
            this.profile_image = '';

            let result = entry.response;

            let data = JSON.parse(result);
            alert('data'+JSON.stringify(data));
            //this.util.showSuccessAlert("Response from server: "+data);
            if(data['error'] == false){

           /*   this.navCtrl.push(MyProfilePage).then(() => {
                const index = this.viewCtrl.index;
                for(let i = index; i > 0; i--){
                  this.navCtrl.remove(i);
                }

              });*/
            }else{
            }
          }, (err) => {

            alert("error"+JSON.stringify(err));
          });
        /*}else{
          alert("error")

       }
      }*/

  }







}
