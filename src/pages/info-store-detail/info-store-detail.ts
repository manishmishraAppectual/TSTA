import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {RestApi} from "../../providers/rest_api";
import {LoadingController} from "ionic-angular/index";

/**
 * Generated class for the InfoStoreDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-info-store-detail',
  templateUrl: 'info-store-detail.html',
})
export class InfoStoreDetailPage {
  loading:any;
  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public navParams: NavParams,
              public restApi:RestApi) {

  //  this.healthtipsDetail();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoStoreDetailPage');
  }




  /*healthtipsDetail() {
    this.showLoader();
    this.restApi.showHealthTips().then((result) => {
      alert(result);
      console.log(result);
      this.loading.dismiss();
      if (result["error"] == false) {
       // this.healthdata=result["healthTips"];

      }
    })
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();

    setTimeout(() => {
      this.loading.dismiss();
    }, 30000);
  }*/



}
