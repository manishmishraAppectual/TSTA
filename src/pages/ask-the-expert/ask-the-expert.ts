import { Component } from '@angular/core';
import {  NavController} from 'ionic-angular';
import {ToastController} from "ionic-angular/index";
import { ViewController } from 'ionic-angular';
import {HomePage} from "../Home/home";
import {Platform} from "ionic-angular/index";
import {TabsPage} from "../tabs-page/tabs-page";
import {App} from "ionic-angular/index";
import {UserData} from "../../providers/user-data";
import {LoadingController} from "ionic-angular/index";
import {RestApi} from "../../providers/rest_api";
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';

let apikey='965b4ff1185114ad588b8e9e874523e2';



@Component({
  selector: 'ask-the-expert',
  templateUrl: 'ask-the-expert.html'
})

export class AskTheExpertPage {
  user: FormGroup;
  step1:boolean=true;
  spec_Detail:any;


  specializationID='';
  loading:any;
  anonymous_user=0;
  userId:any;
  apiKey:any;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public userData: UserData,
    public app: App,
    public restApi:RestApi
  ) {
  this.getPageName();
    this.userData.getLanguage().then((language) => {
      if(language==null || language=='' ){
        this.specilization('en');
      }
      else{
        this.specilization(language);
      }
    });

    this.userData.getUsername().then((val) => {
      if(val==null || val=='' ){
        this.apiKey=apikey;
      }
      else{
        // this.userObj=val;
        this.apiKey=val.api_Key;
        this.userId=val.user_id;
        console.log(val);
      }
    });
  }


  getPageName() {
    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
   });
  }

    selectChange(e) {
        console.log(e);
    }

  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
       if (page == "AskTheExpertPage") {
        this.platform.registerBackButtonAction(()=> {
          let nav = this.app.getRootNav();
          nav.setRoot(TabsPage, {tabIndex: 0});
        });
      }
    });
  }

  ionViewDidLoad() {
    this.getPageName();
    this.step1=true;
  }


  step2(){
    this.step1=false;
  }

  spec_data(specialization_id){
    this.specializationID=specialization_id;
  }


  specilization(language) {
    this.showLoader();
    this.restApi.getSpecilization(language).then((result) => {
      console.log(result);
      this.loading.dismiss();
      if (result["error"] == false) {
        this.spec_Detail=result["Specialization_data"];
        console.log(this.spec_Detail);
      }
    })
  }


  ngOnInit() {
    this.user = new FormGroup({
      userQue: new FormControl('', [Validators.required, Validators.minLength(1)]),
      anonymousUser:new FormControl(false)
    });
  }


  submitQue(user){
    console.log(user.value);
    this.restApi.askTheExpert(user.value,this.specializationID,this.userId).then((result) => {
      console.log(result);
      this.loading.dismiss();
      this.presentToast(result["message"]);
      if (result["error"] == false) {
        this.step1=true;
        this.specializationID='';
        this.dataClear();
      }
    })
  }


  dataClear(){
    this.user = new FormGroup({
      userQue: new FormControl('', [Validators.required, Validators.minLength(1)]),
      anonymousUser:new FormControl(false)
    });
  }


  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
    setTimeout(() => {
      this.loading.dismiss();
    }, 30000);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.onDidDismiss(() => {
    });
    toast.present();
  }

}
