import { Component } from '@angular/core';
import { Platform,NavController } from 'ionic-angular';
import {RestApi} from "../../providers/rest_api";
import {HomePage} from "../Home/home";
import {LoadingController} from "ionic-angular/index";
import {UserData} from "../../providers/user-data";
import {ToastController} from "ionic-angular/index";
import {Events} from "ionic-angular/index";
import {TabsPage} from "../tabs-page/tabs-page";
import {App} from "ionic-angular/index";
import { ViewController } from 'ionic-angular';



let apikey='965b4ff1185114ad588b8e9e874523e2';

@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html'
})



export class QuizPage {
  quiz_data={quiz_question:'',quiz_options:''};
  public quizQuestion:any = [];
  quizAns:any=[];
  button='';
  que_no=0;
  thankYou:boolean=false;
  setQueLength:any;
  quizId:any;
  loading:any;
  userID:any;
  quiz_id:any;
  apiKey:any;
/*
  appPages: PageInterface[] = [
    { title: 'Home', name: 'TabsPage', component: TabsPage, tabComponent: HomePage, index: 0, icon: 'calendar' },
    { title: 'Ask the Expert', name: 'TabsPage', component: TabsPage, tabComponent: AskTheExpertPage, index: 1, icon: 'contacts' },
    { title: 'My Notification', name: 'TabsPage', component: TabsPage, tabComponent: MyNotificationPage, index: 2, icon: 'information-circle' },
    { title: 'Quiz', name: 'TabsPage', component: TabsPage, tabComponent: StartQuizPage, index: 3, icon: 'map' },
    { title: 'My Profile', name: 'TabsPage', component: TabsPage, tabComponent: MyProfilePage, index: 4, icon: 'information-circle' }
  ];*/


  constructor( public platform: Platform,
               public restApi:RestApi,
               public navCtrl:NavController,
               public loadingCtrl:LoadingController,
               public userData: UserData,
               public toastCtrl:ToastController,
               public events: Events,
               public app: App
  ) {
    this.quizData();
    this.userData.getUsername().then((val) => {
      if(val==null || val=='' ){
        this.apiKey=apikey;
      }
      else{
       this.userID=val.user_id;
        console.log(this.userID);
        this.apiKey=val.api_Key;
        console.log(this.apiKey);
      }
    });
    this.getPageName();
  }

  getPageName() {
    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
    });
  }


  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
      if (page == "QuizPage") {
        this.platform.registerBackButtonAction(()=> {
          let nav = this.app.getRootNav();
          nav.setRoot(TabsPage, {tabIndex: 3});
        });
      }
    });
  }

  ionViewDidLoad() {
    this.getPageName();
  }

  quizData() {
    this.showLoader();
    this.restApi.quizSet().then((result) => {
      this.loading.dismiss();
      if (result["error"] == false) {
        this.quiz_id=result["quiz"][0].quiz_id;
          for(let quizs of result["quiz"]) {
         this.quizQuestion.push(quizs);
        }
        this.que_set();
        this.setQueLength=this.quizQuestion.length;
      }
    })
  }

  que_set(){
    this.quiz_data=this.quizQuestion[this.que_no];
      if(this.que_no==10){
      this.thankYou=true;
      let data=JSON.stringify(this.quizAns);
      this.quizAns=data.replace(/[{}]/g, "");
      this.quizAns = this.quizAns.replace("[", "{");
      this.quizAns = this.quizAns.replace("]", "}");
      this.postQuizReplays(this.quizAns);
    }
  }

  postQuizReplays(quizAns){
    this.showLoader();
    this.restApi.postQuizAns(this.userID,this.quiz_id,quizAns).then((result) => {
      this.loading.dismiss();
      this.presentToast(result["message"]);
      if (result["error"] == false) {
      }
    })

  };


  home(){
   // this.events.publish('user:HomePage');
    //this.viewCtrl.dismiss();
    let nav = this.app.getRootNav();
    nav.setRoot(TabsPage, {tabIndex: 0});
  }

  selectAnswer(activ,q_id){
    this.button=activ;
    this.quizId=q_id;
  }

  nextQue(){
    if(this.button!=''){
      let  pusheditems={};
      pusheditems[this.quizId] = this.button;
      this.quizAns.push(pusheditems);
    /*  str.replace(/[{}]/g, "");*/
        this.que_no++;
        if(this.quizQuestion.length>=this.que_no) {
        console.log(this.quizQuestion.length);
        console.log(this.que_no);
        this.que_set();
        this.button = '';
      }
    }
    else{
      alert("please select ans");
    }
  }


  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
    setTimeout(() => {
      this.loading.dismiss();
    }, 30000);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    toast.onDidDismiss(() => {
    });
    toast.present();
  }
}
