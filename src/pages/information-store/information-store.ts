import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {RestApi} from "../../providers/rest_api";

/**
 * Generated class for the InformationStorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-information-store',
  templateUrl: 'information-store.html',
})
export class InformationStorePage {
  infoStoreData:any;
  infoSubDetail:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public restApi:RestApi)
  {
    this.infoStoreData=navParams.get("infoData");
    console.log(this.infoStoreData);
    this.infoDetailData(this.infoStoreData);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformationStorePage');
  }

  infoDetailData(infoStoreData){
    this.restApi.storeDetail(infoStoreData.tips_id).then((result)=>{
      if(result["error"]==false){
        console.log(result);
        this.infoSubDetail=result["infoStoreSubDetail"];
      }
    })
  }
 }
