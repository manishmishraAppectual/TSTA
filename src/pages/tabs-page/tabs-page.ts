import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import {HomePage} from "../Home/home";
import {AskTheExpertPage} from "../ask-the-expert/ask-the-expert";
import {QuizPage} from "../quiz/quiz";
import {MyNotificationPage} from "../my-notification/my-notification";
import { TranslateService } from '@ngx-translate/core';
import {MyProfilePage} from "../my-profile/my-profile";
import {StartQuizPage} from "../start-quiz/start-quiz";
import {MyQuestionsPage} from "../my-questions/my-questions";



@Component({
  templateUrl: 'tabs-page.html'
})

export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = HomePage;
  tab2Root: any = MyQuestionsPage;
  tab3Root: any = StartQuizPage;
  tab4Root: any = MyProfilePage;
  tab5Root: any = AskTheExpertPage;
  mySelectedIndex: number;

  constructor(navParams: NavParams,
              translate: TranslateService) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

}
