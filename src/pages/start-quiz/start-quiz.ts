import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {InfoStoreDetailPage} from "../info-store-detail/info-store-detail";
import {QuizPage} from "../quiz/quiz";
import {TabsPage} from "../tabs-page/tabs-page";
import {ModalController} from "ionic-angular/index";
import { ViewController } from 'ionic-angular';
import {Platform} from "ionic-angular/index";
import {App} from "ionic-angular/index";


/**
 * Generated class for the StartQuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-start-quiz',
  templateUrl: 'start-quiz.html',
})
export class StartQuizPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              public app: App
  ) {
    this.getPageName();


  }

  getPageName() {
    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
    });
  }


  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
      if (page == "StartQuizPage") {
        this.platform.registerBackButtonAction(()=> {
          let nav = this.app.getRootNav();
          nav.setRoot(TabsPage, {tabIndex: 0});
        });
      }
    });
  }

  ionViewDidLoad() {
    this.getPageName();
  }

    startQuiz(){

/*      let quiz = this.modalCtrl.create(QuizPage);
      quiz.present();*/
        // this.navCtrl.goToRoot(InfoStoreDetailPage);
        this.navCtrl.push(QuizPage);
    }

}
