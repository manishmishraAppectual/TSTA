import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {InformationStorePage} from "../information-store/information-store";
import {ViewController} from "ionic-angular/index";
import {Platform} from "ionic-angular/index";
import {App} from "ionic-angular/index";
import {TabsPage} from "../tabs-page/tabs-page";

/**
 * Generated class for the MyQuestionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-questions',
  templateUrl: 'my-questions.html',
})
export class MyQuestionsPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              public app: App) {
    this.getPageName();

  }

  getPageName() {
    this.navCtrl.viewDidEnter.subscribe(item=> {
      const viewController = item as ViewController;
      const page = viewController.name;
      this.back_H_ButtonAction(page);
    });
  }


  back_H_ButtonAction(page) {
    this.platform.ready().then(() => {
      if (page == "MyQuestionsPage") {
        this.platform.registerBackButtonAction(()=> {
          let nav = this.app.getRootNav();
          nav.setRoot(TabsPage, {tabIndex: 0});
        });
      }
    });
  }

  ionViewDidLoad() {
    this.getPageName();
  }

  infoStoreClick() {
      this.navCtrl.push(InformationStorePage);
  }

}
