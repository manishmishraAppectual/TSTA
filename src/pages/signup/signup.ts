import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import { TabsPage } from '../tabs-page/tabs-page';
import {  OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators,ValidatorFn,AbstractControl } from '@angular/forms';
import {ToastController} from "ionic-angular/index";
import {LoadingController} from "ionic-angular/index";
import {RestApi} from "../../providers/rest_api";
import {StartAppPage} from "../start-app/start-app";



@Component({
  selector: 'page-user',
  templateUrl: 'signup.html'
})
export class SignupPage {
  user: FormGroup;
 // signup: UserOptions = { email: '', password: '' };
  submitted = false;
  loading:any;
  constructor(public navCtrl: NavController,
              public userData: UserData,
              public loadingCtrl: LoadingController,
              public restApi:RestApi,
              private toastCtrl: ToastController) {}


  /*ngOnInit() {
    this.signup = new FormGroup({
      firstname: new FormControl('', [Validators.required, Validators.minLength(4)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(4)]),

      email: new FormControl('', [Validators.required,Validators.email]),
      mob: new FormControl('', [Validators.required, Validators.minLength(10),Validators.maxLength(10)])
    });

  }*/


  ngOnInit() {
    this.user = new FormGroup({
      firstname: new FormControl('', [Validators.required, Validators.minLength(3)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(3)]),
      email: new FormControl('', [Validators.required,Validators.email]),
      mob: new FormControl('', [Validators.required, Validators.minLength(10),Validators.maxLength(10)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }


  onSubmit(user){
    let data=user.value;
    console.log(user.value);
    if(data.password==data.confirmPassword){
      this.callApi(data);
    }
    else{
      this.presentToast('password not matched');
    }

  }




  callApi(user){
      this.showLoader();
      this.restApi.createUser(user).then((result) => {
        //    alert(JSON.stringify(result));
        if(result["error"] == false){
          this.loading.dismiss();
          this.userData.login(result["user"]);
          //noinspection TypeScriptValidateTypes
          this.navCtrl.push(TabsPage);
        }else{
          this.loading.dismiss();
          this.presentToast(result["message"]);
        }
      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
      });
    }

    showLoader(){
      this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
      });
      this.loading.present();
    }

    presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 5000,
        position: 'bottom',
        dismissOnPageChange: true
      });

      toast.onDidDismiss(() => {
      });
      toast.present();
    }


    backto_startapp(){
        this.navCtrl.push(StartAppPage);
    }




}
