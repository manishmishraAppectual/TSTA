import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { UserData } from '../../providers/user-data';

/**
 * Generated class for the ChangeLanguagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-language',
  templateUrl: 'change-language.html',
})
export class ChangeLanguagePage {
  lang:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userData: UserData,
              public translate: TranslateService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeLanguagePage');
  }


  switchLanguage() {
    console.log(this.lang);
    this.userData.changeLng(this.lang);
    this.translate.use(this.lang);
  }
}
