import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';

let apiUrl = 'http://172.104.54.149/toShytoAsk/even_rest_api/v1/index.php/';

@Injectable()
export class RestApi {




    constructor(public http:Http) {
    }



    loginApi(credentials) {
        return new Promise((resolve, reject) => {
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;' );
            //noinspection TypeScriptValidateTypes
            let options = new RequestOptions({ headers: headers });
            let body = 'email='+credentials.username +'&password='+credentials.password+'&auth_type='+'0';
            //noinspection TypeScriptValidateTypes
            console.log(JSON.stringify(credentials));
            this.http.post(apiUrl+'login', body, options)
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
            });
    }

       socilLogin(credentials) {
        return new Promise((resolve, reject) => {
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;' );
            //noinspection TypeScriptValidateTypes
            let options = new RequestOptions({ headers: headers });
            let body = 'first_name='+credentials.name +'&last_name='+''+'&phone='+null+'&email='+credentials.email +'&password='+''+
                '&device_type='+''+'&device_token='+'' +'&auth_type=' +'1' +'&device_id='+''+'&device_model='+''+'&device_version='+''+'&profile_pic='+'';
            //noinspection TypeScriptValidateTypes
            console.log(JSON.stringify(credentials));
            this.http.post(apiUrl+'register', body, options)
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });
    }


      createUser(credentials) {
        return new Promise((resolve, reject) => {
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;' );
            //noinspection TypeScriptValidateTypes
            let options = new RequestOptions({ headers: headers });
            let body = 'first_name='+credentials.firstname +'&last_name='+credentials.lastname+'&phone='+credentials.mob+'&email='+credentials.email +'&password='+credentials.password+
                '&device_type='+'android'+'&device_token='+'' +'&auth_type=' +'0' +'&device_id='+''+'&device_model='+''+'&device_version='+''+'&profile_pic='+'';
            //noinspection TypeScriptValidateTypes
            console.log(JSON.stringify(credentials));
            this.http.post(apiUrl+'register', body, options)
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });
    }


    healtTips(language,apiKey) {
                return new Promise((resolve, reject) => {
         let headers = new Headers();
         headers["Access-Control-Allow-Origin"] = "*";
         headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT";
         headers["Content-type"] = "application/json";
         headers["Accept"] = "application/json";
         //noinspection TypeScriptValidateTypes
         //  let body = 'lng_id='+language;
         this.http.get(apiUrl + 'healthtips?lng_id='+language, {headers: headers})
         .subscribe(res => {
         resolve(res.json());
         }, (err) => {
         reject(err);
         });
         });


        /*return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('APIKEY',apiKey);

            //headers.append('Authorization', `Bearer ${authToken}`);

            //noinspection TypeScriptValidateTypes
            this.http.get(apiUrl+'health_tips?lng_id='+language,{headers: headers})
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });*/
    }


    dashSliderData(language,apiKey) {
        return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers["Access-Control-Allow-Origin"] = "*";
            headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT";
            headers["Content-type"] = "application/json";
            headers["Accept"] = "application/json";
            //noinspection TypeScriptValidateTypes
            //  let body = 'lng_id='+language;
            this.http.get(apiUrl + 'dashSlider?lng_id='+language, {headers: headers})
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });

        /*return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
            headers.append('Accept', 'application/json');
            headers.append('APIKEY',apiKey);

            //headers.append('Authorization', `Bearer ${authToken}`);

            //noinspection TypeScriptValidateTypes
            this.http.get(apiUrl+'dash_slider?lng_id='+language,{headers: headers})
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });*/


    }



    quizSet() {
        return new Promise((resolve, reject) => {
         let headers = new Headers();
         headers["Access-Control-Allow-Origin"] = "*";
         headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT";
         headers["Content-type"] = "application/json";
         headers["Accept"] = "application/json";
         //noinspection TypeScriptValidateTypes
         //  let body = 'lng_id='+language;
         this.http.get(apiUrl + 'quiz', {headers: headers})
         .subscribe(res => {
         resolve(res.json());
         }, (err) => {
         reject(err);
         });
         });
    }




    postQuizAns(userID,quiz_id,quizAns) {
        return new Promise((resolve, reject) => {
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;' );
            //noinspection TypeScriptValidateTypes
            let options = new RequestOptions({ headers: headers });
            let body = 'quiz_id='+quiz_id +'&user_id='+userID+'&answer='+quizAns;
            //noinspection TypeScriptValidateTypes
            this.http.post(apiUrl+'quiz_reply', body, options)
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });
    }


   getSpecilization(language){
       return new Promise((resolve, reject) => {
           let headers = new Headers();
           headers["Access-Control-Allow-Origin"] = "*";
           headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT";
           headers["Content-type"] = "application/json";
           headers["Accept"] = "application/json";

           this.http.get(apiUrl + 'specialization?lng_id='+language, {headers: headers})
               .subscribe(res => {
                   resolve(res.json());
               }, (err) => {
                   reject(err);
               });
       })
   }

    askTheExpert(crediantals,specializationID,userId){
        return new Promise((resolve, reject) => {
            var headers = new Headers();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;' );
            //noinspection TypeScriptValidateTypes
            let options = new RequestOptions({ headers: headers });
            console.log(crediantals);
            let anonymousUSer;
            if(crediantals.anonymousUser){
                anonymousUSer=1;
            }else{
                anonymousUSer=0;
            }

            let body = 'user_id='+userId +'&specialization_id='+specializationID+'&question='+crediantals.userQue+'&view_user='+anonymousUSer;
            //noinspection TypeScriptValidateTypes
            this.http.post(apiUrl+'ask_the_expert', body, options)
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        });

    }




    storeDetail(tips_id){

        return new Promise((resolve,reject)=>{
            let headers = new Headers();
            headers["Access-Control-Allow-Origin"] = "*";
            headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, PUT";
            headers["Content-type"] = "application/json";
            headers["Accept"] = "application/json";

            this.http.get(apiUrl + 'inforamationStoreHouse?tipsID='+tips_id, {headers: headers})
                .subscribe(res => {
                    resolve(res.json());
                }, (err) => {
                    reject(err);
                });
        })

    }

    /*  showHealthTips() {
          return new Promise((resolve, reject) => {
          let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          headers.append('Access-Control-Allow-Origin', '*');
          headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
          headers.append('Accept', 'application/json');
          headers.append('Authorization','Basic aGVhbHRoY2FyZUBnbWFpbC5jb206TmV1cm9AMTIz');
          //headers.append('Authorization', `Bearer ${authToken}`);

          //noinspection TypeScriptValidateTypes
          this.http.get('http://wellness.metropolisindia.com/healthcare/api/users/healthtips_detail/6', {headers: headers})
              .subscribe(res => {

                  resolve(res().json);
              }, (err) => {

                  reject(err);
              });

      });
      }*/






}